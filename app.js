var express = require('express'),
    mongoose = require('mongoose'),
    bodyParser = require('body-parser');
var path = require('path');
var cookieParser = require('cookie-parser');
var ejsLayouts = require("express-ejs-layouts");

config = require('./config/config.js');
app = express();

app.use(express.static('public'));
app.use('/favicon.ico', express.static('public/favicon.ico'));

app.use(cookieParser());

app.use(bodyParser.json({defer: true}));
                app.use(bodyParser.urlencoded({
                    extended: true,
                    defer: true
                }));

app.use(ejsLayouts);

/*
app.use(function(req, res, next) { // local testing doesnt have https
	if (req.get('Host') == "localhost:8081" || req.get('Host') == "10.0.0.2:8081"){
		next();
	}else{
		if((!req.secure) && (req.get('X-Forwarded-Proto') !== 'https')) {
			res.redirect('https://' + req.get('Host') + req.url);
		}
		else
			next();
	}
});
*/

app.set('view engine', 'ejs');
require('./routes/main_routes').mainRoutes();

mongoose.connect(config.appconf.db, config.appconf.db_options, function (err) {
	if(err){
		console.log('Error connecting to db',err);
	}else{
        console.log('connected to db');
  }
});



config.appconf.bind_port = process.env.PORT || config.appconf.bind_port;
console.log("Start Helping Institute API server @ " + config.appconf.bind_port);
app.listen(config.appconf.bind_port);

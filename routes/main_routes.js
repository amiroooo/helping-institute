'use strict';
var express = require('express'),
	mainCtrl=require('../controllers/main_controller.js');


exports.mainRoutes = function() {


	app.get('/', mainCtrl.renderHomePage);
	app.get('/additems', mainCtrl.renderDataPage);
	app.post('/additems', mainCtrl.requestItemCreation);
	
	//app.get('/coming_soon', mainCtrl.renderComingSoon);
	
	app.get('/project/:id', mainCtrl.renderProjectPage);
	
	app.get('/project/income/:id', mainCtrl.renderIncomePage);
	app.get('/project/outcome/:id', mainCtrl.renderOutcomePage);
	
	app.post('/project/cascade/:id', mainCtrl.renderCascadePage);
	
	//app.post('/request_update_post', mainCtrl.pageRequiresAdmin, mainCtrl.requestUpdatePost);
	//app.post('/admin/add_option', mainCtrl.pageRequiresAdmin, mainCtrl.requestAddOption); // TODO TESTING
	//app.post('/admin/post_to_fb', mainCtrl.pageRequiresAdmin, mainCtrl.autoPostVideoById);
	
	
};

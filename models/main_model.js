'use strict';

/**
 * Module dependencies.
 */
var mongoose = require('mongoose'),
    Schema = mongoose.Schema;


var ProjectSchema = new Schema({
    projectName: {
        type: String,
        required: true,
        unique: true
    },
    description: {
        type: String
    },
    projectLogo: {
        type: String
    },
    createDate: {
        type: Date,
        required: true
    },
    ownerName: {
        type: String,
        required: true
    }
});




var UserSchema = new Schema({
    userName: {
        type: String,
		required: true
    },	
    userLocation: {
        type: String,
        required: true
    }
});



var TransferSchema = new Schema({
	description: {
        type: String,
		required: true
    },
    type: {
        type: String,
        required: true,
        enum: ['income','outcome']
    },
    projectId: {
        type: Schema.Types.ObjectId,
		ref: 'Project',
        required: true
    },
	performerId: {
        type: Schema.Types.ObjectId,
		ref: 'User'
    },
	sum: {
		type: Number,
        required: true
	},
	sumCompleted: {
		type: Number
	},
    createDate: {
        type: Date,
        required: true
    }
	

});





//TODO add dependancy on delete project

mongoose.model('Project', ProjectSchema);
mongoose.model('User', UserSchema);
mongoose.model('Transfer', TransferSchema);




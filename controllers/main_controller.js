var async = require('async'),
    VideoModel = require('../models/main_model.js');
	mongoose = require('mongoose'),
    Project = mongoose.model('Project'),
	User = mongoose.model('User'),
	Transfer = mongoose.model('Transfer'),
	
    config = require('../config/config.js'),
    MongoClient = require('mongodb').MongoClient;
	
var fs = require('fs'),
    path = require('path'),    
    filePath = path.join(__dirname, '../presents2.txt');

exports.renderProjectPage = async function(req, res){
	var id = req.params.id;
	//var locations = await exports.getProjectDonatorLocations(id);
	var alltransfers = await exports.getTransfers(id);
	var nextStep = await exports.getLessDifferentAttributeTypeAndResults(alltransfers,["createDate","sum","projectId"],0);
	var nextStage;
	if (nextStep != undefined){
		nextStage = await exports.buidNextStage(nextStep);
	}
	
	Project.findOne({'_id': id}).exec(function (err, project) {
		if (err){
			return res.send("Error retrieving Project: " + err);
		}else{
			return res.render('showProject', {
				 "project": project,
				 "title": "Project Page",
				 //"locations": locations,
				 "nextStage": nextStage,
				 nav_menu:exports.getNavArray(req)
			});
		}
	});
}

exports.renderHomePage = function (req, res, extra) {
	Project.find({}).exec(function(err, projects){
		var json = {"title" : "Home Page", "nav_menu" : exports.getNavArray(req)};
		for (var key in extra) {
			if (extra.hasOwnProperty(key)) {
				json[key] = extra[key];
			}
		}
		if (!err){
			json.projects = projects;
		}
		res.render("welcome", json);
	});
}


exports.renderGraphPage = function (req, res) {
	var id = req.query.id;
	var type = req.query.type;
	res.render("showGraph",{"title" : "Graph Page", "nav_menu" : exports.getNavArray(req)});	
}


exports.addParamFieldsFromReqToArray = async function(req,exclusion){
	var data = req.body;
	for (var key in data) {
		if (data.hasOwnProperty(key)) {
			if (exclusion.indexOf(key) == -1){
				exclusion.push(key);
			}
		}
	}
	return exclusion; 	
}

function getFieldUnit(fieldName){
	switch (fieldName.toString()){
		case "sum":{
			return "Shekel";
		}
		case "description":{
			return "Contributions";
		}
		case "performerId,userLocation":{
			return "Donators";
		}
		default:{
			console.log("getFieldUnit def: !" + fieldName + "!");
			return "Transactions";
		}
	}
}



exports.renderCascadePage = async function (req, res) {
	var id = req.params.id;
	//var filterByLocation = req.query.filterLoc;
	//var locPercentage = req.query.locPercentage;
	var quey_data = req.body;
	var query_length = Object.keys(quey_data).length;
	
	var filters = await exports.addVisibleDetailsToQuery(quey_data);
	var changeablequery = JSON.parse(JSON.stringify(quey_data));
	var exclusion = ["createDate","projectId","sum"];
	exclusion = await exports.addParamFieldsFromReqToArray(req,exclusion);
	
	var transfers = await exports.getTransfers(id, changeablequery);
	var total = await exports.totalTransfersSum(transfers);
	var nextStep = await exports.getLessDifferentAttributeTypeAndResults(transfers,exclusion,1);
	var unit = "Shekel";
	var nextStage;
	if (nextStep != undefined){
		nextStage = await exports.buidNextStage(nextStep);
		console.log();
		unit = await getFieldUnit(nextStep.fieldName);
	}
	
	Project.findOne({'_id': id}).exec(function (err, project) {
		if (err){
			return exports.renderErrorPage(res, "Error", "Error retrieving Project: " + err);
		}else{
			return res.render('showCakeGraph', {
				 "project": project,
				 "title": "Project Income Page",
				 "ttype":"Filter",
				 "year": project.createDate.getFullYear(),
				 "transfers": transfers,
				 "total": total,
				 "unit" : unit,
				 "quey_data":quey_data,
				 "filters":filters,
				 "query_length":query_length,
				 "backCount":0,
				 "nextStage":nextStage,
				 "nav_menu": exports.getNavArray(req)
			});
		}
	});	
}

exports.renderIncomePage = async function (req, res) {
	var id = req.params.id;
	var filterByLocation = req.query.filterLoc;
	var locPercentage = req.query.locPercentage;
	var transfers;
	if (filterByLocation != undefined){
		transfers = await exports.getIncomeTransfersByLocation(id,filterByLocation);
	}else{
		var alltransfers = await exports.getIncomeTransfers(id);
		if (locPercentage){
			transfers = await exports.calculatePercentageByLocs(alltransfers);
			console.log(transfers);
		}else{
			transfers = alltransfers;
		}
	}
	var total = await exports.totalTransfersSum(transfers);
	Project.findOne({'_id': id}).exec(function (err, project) {
		if (err){
			return exports.renderErrorPage(res, "Error", "Error retrieving Project: " + err);
		}else{
			return res.render('showCakeGraph', {
				 "project": project,
				 "title": "Project Income Page",
				 "ttype":"Income",
				 "year": project.createDate.getFullYear(),
				 "transfers": transfers,
				 "total": total,
				 "nav_menu": exports.getNavArray(req)
			});
		}
	});	
}




exports.renderOutcomePage = async function (req, res) {
	var id = req.params.id;
	var transfers = await exports.getOutcomeTransfers(id);
	var total = await exports.totalTransfersSum(transfers);
	Project.findOne({'_id': id}).exec(function (err, project) {
		if (err){
			return exports.renderErrorPage(res, "Error", "Error retrieving Project: " + err);
		}else{
			return res.render('showCakeGraph', {
				 "project": project,
				 "title": "Project Outcome Page",
				 "transfers": transfers,
				 "ttype":"Outcome",
				 "year": project.createDate.getFullYear(),
				 "total": total,
				 "nav_menu": exports.getNavArray(req)
			});
		}
	});	
}


exports.renderDataPage = async function (req, res, extra) {
	var users = await exports.getUsers();	
	Project.find({}).exec(function(err, projects){
		var json = {"title" : "Add Data To Database Page","users": users, "nav_menu" : exports.getNavArray(req)};
		for (var key in extra) {
			if (extra.hasOwnProperty(key)) {
				json[key] = extra[key];
			}
		}
		if (!err){
			json.projects = projects;
		}
		res.render("addItems", json);
	});
}


exports.renderErrorPage = function (res, title, info){
	res.render("error_page", {"title" : title,"error_text": info, "nav_menu" : exports.getNavArray(res)});
}



exports.requestItemCreation = async function (req, res) {
	var rtype = req.body.rtype;
	switch (rtype) {
		case "project":{
			var minimalFieldsAvailable = await exports.assertJsonFields(req.body, ["projectName","ownerName"]);
			if (minimalFieldsAvailable){
				var project =  new Project();
				project.projectName = req.body.projectName;
				project.ownerName = req.body.ownerName;
				project.createDate = new Date();
				if (req.body.description != undefined){
					project.description = req.body.description;
				}
				if (req.body.projectLogo != undefined){
					project.projectLogo = req.body.projectLogo;
				}
				project.save(function (err, savedPost) {
					if (err){
						console.log("Project creation failed.");
						exports.renderDataPage(req, res, exports.getDefaultAlert("wrong"));
					}else{
						console.log("Project creation success.");
						exports.renderDataPage(req, res, exports.getDefaultAlert("success"));
					}
				});
			}else{
				exports.renderDataPage(req, res, exports.getDefaultAlert("obligatory"));// todo error page
			}
			break;
		}
		case "user":{
			var minimalFieldsAvailable = await exports.assertJsonFields(req.body, ["userName","userLocation"]);
			if (minimalFieldsAvailable){
				var user =  new User();
				user.userName = req.body.userName;
				user.userLocation = req.body.userLocation;
				user.save(function (err, savedUser) {
					if (err){
						console.log("User creation failed.");
						exports.renderDataPage(req, res, exports.getDefaultAlert("wrong"));
					}else{
						console.log("User creation success.");
						exports.renderDataPage(req, res, exports.getDefaultAlert("success"));
					}
				});
			}else{
				exports.renderDataPage(req, res, exports.getDefaultAlert("obligatory"));// todo error page
			}
			break;
		}
		case "transfer":{
			var minimalFieldsAvailable = await exports.assertJsonFields(req.body, ["description","type","projectId","sum"]);
			if (minimalFieldsAvailable){
				var transfer =  new Transfer();
				transfer.description = req.body.description;
				transfer.type = req.body.type;
				transfer.projectId = req.body.projectId;
				transfer.sum = req.body.sum;
				transfer.createDate = new Date();
				if (req.body.performerId != undefined && req.body.performerId != ""){
					transfer.performerId = req.body.performerId;
				}
				transfer.save(function (err, savedTransfer) {
					if (err){
						console.log("Transfer creation failed." + err);
						exports.renderDataPage(req, res, exports.getDefaultAlert("wrong"));
					}else{
						console.log("Transfer creation success.");
						exports.renderDataPage(req, res, exports.getDefaultAlert("success"));
					}
				});
			}else{
				exports.renderDataPage(req, res, exports.getDefaultAlert("obligatory"));// todo error page
			}
			break;
		}
	}
	
}




//************************UTILS***********************

exports.getNavArray = function(req){
	var menu_array = [{url:"/", name:"Home", name:"Home Page"}];
	menu_array.push({url:"/additems", name:"Add Data To Database Page"});
	return menu_array;
}

function randomIntInc(low, high) {
    return Math.floor(Math.random() * (high - low) + low);
}


exports.getAlertJson = function(info, type){
	if (type == undefined){
		type = "info"
	}
	return {"notice": info, "notice_type": type};
}

exports.getDefaultAlert = function(id){
	switch(id){
		case "obligatory":{
			return exports.getAlertJson("Obligatory fields for Transfer creation missing.","danger")
		}
		case "wrong":{
			return exports.getAlertJson("Something went wrong.","danger")
		}
		case "success":{
			return exports.getAlertJson("Request executed successfuly.","success")
		}
		default: {
			return {};
		}
	}
}

exports.assertJsonFields = async function(json, req_arr){
	return new Promise(function(resolve, reject) {
		async.each(req_arr, function(key, callback) {
		if (json[key] == undefined){
			callback("no item");
		}else{
			callback();
		}
	  }, function(err) {
		  if( err ) {
			resolve(false);
		  } else {
			resolve(true);
		  }
	  });
	});
}


exports.getUsers = function(){
		return new Promise(function(resolve, reject) {
		  User.find({}).exec(
			function (err, users) {
				if (err){
					resolve({});
				}else{
					resolve(users);
				}
			})
	});
}
exports.totalTransfersSum = async function(transfers){
	var total = 0;
	return new Promise(function(resolve, reject) {
		async.each(transfers, function(transfer, callback) {
		if (transfer == undefined){
			callback("error");
		}else{
			total = total + transfer.sum;
			callback();
		}
	  }, function(err) {
		  if( err ) {
			resolve(total);
		  } else {
			resolve(total);
		  }
	  });
	});
}


exports.calculatePercentageByLocs = async function(transfers){
	return new Promise(function(resolve, reject) {
		var locPercentages = [];
		async.each(transfers, async function(transfer) {
								if (transfer != undefined){			
									var locObj;
									if (transfer.performerId){
										locObj = transfer.performerId.userLocation;
									}
									var locationName;
									if (locObj){
										locationName = locObj.toString();
									}
									if (locationName){
										if (locPercentages[locationName] != undefined){
											locPercentages[locationName] += transfer.sum;
										}else{
											locPercentages[locationName] = transfer.sum;
										}
									}else{
										if (locPercentages["No Location"] != undefined){
											locPercentages["No Location"] += transfer.sum;
										}else{
											locPercentages["No Location"] = transfer.sum;
										}
									}
								}									
							}, function(err) {
								if( err ) {
										console.log("Error looping for filtered array: " + err);
										//resolve(locPercentages);
								} else {
										var array = [];
										for (var key in locPercentages) {
											if (locPercentages.hasOwnProperty(key)) {
											array.push({"description":key,"sum":locPercentages[key],"queryLink":"incomeByLoc"});
											}
										}
										resolve(array);
								}
							}
		);
	});
}



function checkDoubleExclusion(exclusionArr,firstField,secondField, done){
	if (exclusionArr.indexOf(secondField)>-1 || exclusionArr.indexOf(firstField)>-1){
		return done(false);
	}
	async.each(exclusionArr, async function(excluded) {
			if (excluded != undefined ){
				var arr = excluded.split(",");
				if (arr[0] == firstField && arr[1] == secondField){
					//console.log("exclusion went to false");
					//resolve(false);
					return done(false);
				}
			}									
	}, function(err) {
			if( err ) {
				console.log("Error looping for filtered array: " + err);
			} else {
				//resolve(true);
				return done(true);
			}
	});
}



exports.getLessDifferentAttributeTypeAndResults = async function(transfers,exclusion,minFields){
	//console.log("exclusion: " +JSON.stringify(exclusion));
	fieldsCountMap = {};
	var stringToFile = "";

	exclusion.push("__v");
	exclusion.push("_id");

	await Transfer.schema.eachPath(function(path) {
		var pathStr = path.toString();
		if (exclusion.indexOf(pathStr)==-1){
			async.each(transfers, function(transfer) {
						if (fieldsCountMap[pathStr] == undefined){
							fieldsCountMap[pathStr] = {};
						}
						if (transfer != undefined && transfer[pathStr] != undefined){
							if (typeof transfer[pathStr] == "object"){
								var nest = transfer[pathStr];
								var firstNest = fieldsCountMap[pathStr];
								for (var kkk in nest) {
									var kkkStr = kkk.toString();
									checkDoubleExclusion(exclusion,pathStr,kkk,function(isAllowedToAddTheValue){
										if (isAllowedToAddTheValue){
											var nestedField = nest[kkkStr].toString();

											if (firstNest[kkkStr] == undefined){
												firstNest[kkkStr] = {};
											}
											
											var secondNest = firstNest[kkkStr];
											if (secondNest[nestedField] == undefined){
												secondNest[nestedField] = 0;
											}
											secondNest[nestedField] += 1;
											
										}
									});
									
								}	
								
							}else if (typeof transfer[pathStr] == "array"){
								
							}else{
								var firstNest = fieldsCountMap[pathStr];
								var temp = transfer[pathStr].toString();
								if (firstNest[temp] == undefined){
									firstNest[temp] = 0;
								}
								firstNest[temp] = firstNest[temp] + 1;
							}
						}
					}, function(err) {
						if( err ) {
							console.log("Error looping for filtered array: " + err);
						} else {
						}
				}
			);
		}
	});
	var arr = await getFieldNameAndListValues (fieldsCountMap, 100,minFields);
	var chosenKey = arr[0];
	var chosenValues = arr[1];
	
	/*
	var wfilePath = path.join(__dirname, '../testJson.txt');
	fs.writeFile(wfilePath, JSON.stringify(fieldsCountMap), {encoding: 'utf-8',flag:"w"}, function(err) {
			if(err) {
					return console.log(err);
			}
	}); 
	*/
	
	if (chosenKey != undefined){
		//console.log("suitable field: " + chosenKey);
		//console.log("suitable chosenValues: " + JSON.stringify(chosenValues));
		return {"fieldName": chosenKey, "fieldValueNames": chosenValues};
	}else{
		return null;
	}

}


async function getFieldNameAndListValues (fieldsCountMapList, chosenCount,minFields){
	if (minFields == undefined){
		minFields = 0;
	}
	var exclusions = ["__v","_id"];
	var chosenKey = [];
	var chosenValues = {};
	for (var key in fieldsCountMapList) {
		var foundArr = false;
		var tempkeys = Object.keys(fieldsCountMapList[key]);
		for (var key2 in fieldsCountMapList[key]) {
			if (typeof fieldsCountMapList[key][key2] == "object"){
				foundArr = true;
				var arr = await getFieldNameAndListValues (fieldsCountMapList[key], chosenCount);				
				if (arr[2]>minFields && arr[2] < chosenCount) {
					 chosenKey = [key].concat(arr[0]);
					 chosenValues = arr[1];
					 chosenCount = arr[2];
				}
				break;
			}else{
				break;
			}
		}
		if (!foundArr && exclusions.indexOf(key) == -1){
				var count = tempkeys.length;
				if (count>0 && count<chosenCount) {
					chosenKey = [];
					chosenKey.push(key.toString());
					chosenValues = {"fields":tempkeys, "data" : fieldsCountMapList[key]};
					chosenCount = count;
				}
		}		
	}
	//console.log("chosenKey before: " + chosenKey +",chosenValues: "+chosenValues+",chosenCount: "+chosenCount);
	return [chosenKey,chosenValues,chosenCount];
}


exports.addVisibleDetailsToQuery = function(query){
	var new_query = {};
	for (key in query)	{
		var new_key = exports.getFieldLabel(key);
		var new_value = exports.getFieldLabel(query[key]);
		new_query[new_key] = new_value;
	}
	return new_query;
}



exports.getFieldLabel =  function(field){
	switch (field){
		case "income":{
			return "Income";
		}
		case "outcome":{
			return "Outcome";
		}
		case "type":{
			return "Transfer Type";
		}
		case "description":{
			return "Destination";
		}
		case "userLocation":{
			return "Location";
		}
		case "performerId,userLocation":{
			return "Location";
		}
		case "sum":{
			return "Transfer Sum";
		}
		case "sumCompleted":{
			return "Completed Transfer Sum";
		}
		default:{
			return field;
		}
	}
}

exports.buidNextStage =  function(nextStep){
	if (nextStep == undefined || nextStep == null){
		return undefined;
	}
	var nextStage = [];
	//console.log("length chosen: "+nextStep["fieldValueNames"].fields.length );
	//console.log("nextStep: "+ JSON.stringify(nextStep));
	if (nextStep["fieldValueNames"].fields != undefined){
		for (var i =0; i < nextStep["fieldValueNames"].fields.length;i++) {
			var key = {'field':nextStep["fieldValueNames"]['fields'][i] , 'sum':nextStep["fieldValueNames"]['data'][nextStep["fieldValueNames"]['fields'][i]]};
			nextStage.push({"fieldName":nextStep["fieldName"],"field":key,"fieldNameDesc": exports.getFieldLabel(nextStep["fieldName"][nextStep["fieldName"].length-1]),"fieldDesc": exports.getFieldLabel(nextStep["fieldValueNames"]['fields'][i])});
		}
	}
	//console.log("nextStage :: " +JSON.stringify(nextStage));
	return nextStage;
}

function cleanArray(actual) {
  var newArray = new Array();
  for (var i = 0; i < actual.length; i++) {
    if (actual[i]) {
      newArray.push(actual[i]);
    }
  }
  return newArray;
}

exports.getTransfers = async function(id,query){
	return new Promise(function(resolve, reject) {
		var populate_query = {'path':'performerId','model':User};
		var populate_check = {};
		
		if (query == undefined){
			query = {};
		}else{
			delete query.submition;
			for (var key in query){
				if (key.indexOf(",")>0){
					var res = key.split(",");
					populate_check[res[1]] = query[key];
					delete query[key];
				}
			}//,match:{"userLocation":"الرملة"},'model':User
		}
		query.projectId = id;
		//console.log("get transfers query: " + JSON.stringify(query));
		//console.log("get transfers populate_check: " + JSON.stringify(populate_check));
		//populate_check = {"userLocation": "USA,Silicon Valley"};
		Transfer.find(query).populate({'path':'performerId'}).lean().exec( 
			async function (err, transfers) {
				if (err){
					resolve([]);
				}else{
					for(i= transfers.length-1;i>=0;i--){
						for(var key in populate_check){
							var transfer = transfers[i];
							var transferfield = transfer['performerId'][key].toString();
							if (transferfield == undefined || transferfield != populate_check[key]){
								delete transfers[i];
							}
						}
					}
					transfers = await cleanArray(transfers);
					//console.log("transfers: " + JSON.stringify(transfers));
					resolve(transfers);
				}
		})
	});
}

exports.getIncomeTransfers = async function(id){
	return new Promise(function(resolve, reject) {
		  Transfer.find({"projectId": id,"type":"income"}).populate({path:'performerId',model:User}).exec(
			function (err, transfers) {
				if (err){
					resolve([]);
				}else{
					resolve(transfers);
				}
			})
	});
}

exports.getIncomeTransfersByLocation = async function(id, locationName){
	return new Promise(function(resolve, reject) {		
		  User.find({"userLocation": locationName}).exec(
			function (err, users) {
				if (err){
					console.log("Error: " +err);
					resolve();
				}else{
					var result = users.map(function(a) {return a._id.toString();});
					var filteredTransfers = [];
					Transfer.find({"projectId": id,"type":"income"}).populate({path:'performerId',model:User}).exec(
						function (err, transfers) {
							if (err){
								resolve([]);
							}else{
								async.each(transfers, async function(transfer) {
										if (transfer != undefined){
											var transactionUserId;
											if (transfer.performerId){
												transactionUserId = transfer.performerId._id.toString();
											}

											if (transfer.performerId && transactionUserId && result.includes(transactionUserId)){
												filteredTransfers.push(transfer);
											}
										}									
								}, function(err) {
									  if( err ) {
										console.log("Error looping for filtered array: " + err);
									  } else {
										resolve(filteredTransfers);
									  }
								});
							}
					})
				}
			})		
		

	});
}




exports.getProjectDonatorLocations = async function(id){
	return new Promise(function(resolve, reject) {		
		var locations = [];
		Transfer.find({"projectId": id,"type":"income"}).populate({path:'performerId',model:User}).exec(
						function (err, transfers) {
							if (err){
								resolve([]);
							}else{
								async.each(transfers, async function(transfer) {
										if (transfer != undefined){
											
											var locObj;
											if (transfer.performerId){
												locObj = transfer.performerId.userLocation;
											}
											var locationName;
											if (locObj){
												locationName = locObj.toString();
											}
											if (locationName && !locations.includes(locationName)){
												locations.push(locationName);
											}
										}									
								}, function(err) {
									  if( err ) {
										console.log("Error looping for filtered array: " + err);
										resolve(locations);
									  } else {
										resolve(locations);
									  }
								});
							}
		})
	});
}





exports.getOutcomeTransfers = async function(id){
		return new Promise(function(resolve, reject) {
		  Transfer.find({"projectId": id,"type":"outcome"}).exec(
			function (err, transfers) {
				if (err){
					resolve([]);
				}else{
					resolve(transfers);
				}
			})
	});
}


exports.addTransactionWithOwner = async function(projectId, userLocation, userName, date1, date2, description, sum, sumCompleted){
		return new Promise(function(resolve, reject) {
			/*
			console.log("projectId: " + projectId);
			console.log("userLocation: " + userLocation);
			console.log("userName: " + userName);
			console.log("date: " + date);
			console.log("description: " + description);
			console.log("sum: " + sum);
			console.log("sumCompleted: " + sumCompleted);
			*/
			
			
			let user = new User({
				"userName": userName,
				"userLocation": userLocation
			});

			User.findOneAndUpdate({
				"userName": userName
			}, user, { "upsert": true, "new": true }, function(err, res) {
				if (!err && res){
					console.log(userName + " user is added with id: " + res._id);
					let transaction = new Transfer({
						"description": description,
						"type": "income",
						"projectId" : projectId,
						"performerId": res._id,
						"sum": sum,
						"createDate": new Date(date1)
					});
					
					
					let transaction2 = new Transfer({
						"description": description,
						"type": "outcome",
						"projectId" : projectId,
						"performerId": res._id,
						"sum": sumCompleted,
						"createDate": new Date(date2)
					});
					
					transaction2.save(function (err, savedTransaction) {
						if (err){
							console.log("error saving transaction: " + err);
						}else{
							
						}
					})					
					transaction.save(function (err, savedTransaction) {
						if (err){
							console.log("error saving transaction: " + err);
						}else{
							
						}
						resolve();
					})
				}
			});
			
			
			
			/*
			var wfilePath = path.join(__dirname, '../test.txt');
			fs.writeFile(wfilePath, "userLocation: " + userLocation + "		" + "userName: " + userName +"		"+ "description: " + description + '\n', {encoding: 'utf-8',flag:"a"}, function(err) {
							if(err) {
								return console.log(err);
							}
					resolve();
			}); 			
			*/
			
	});
}




exports.parseUnicodeTextTableToNewProject = function(projectName, description, ownerName, projectLogo){

				var project =  new Project();
				project.projectName = projectName;
				project.ownerName = ownerName;
				project.createDate = new Date();
				if (description != undefined){
					project.description = description;
				}
				if (projectLogo != undefined){
					project.projectLogo = projectLogo;
				}else{
					project.projectLogo = "";
				}
				project.save(function (err, savedProject) {
					if (err){
						console.log("Project creation failed.");
					}else{
						
						var projectId = savedProject._id;						
						fs.readFile(filePath, {encoding: 'utf-8'}, function(err, data){
							if (!err) {
								var arrayOfRows = data.split('\n');
								var firstLinePassed = false;
								async.each(arrayOfRows, async function(row) {
									if (!firstLinePassed){
										firstLinePassed = true;
									}else{
										if (row != undefined){
											var arrayOfColumns = row.split('	');
											if (arrayOfColumns.length >= 11){
												console.log("projId inside id: " + projectId);
												var j = await exports.addTransactionWithOwner(projectId, arrayOfColumns[0],arrayOfColumns[4],arrayOfColumns[10],arrayOfColumns[11],arrayOfColumns[9],arrayOfColumns[5],arrayOfColumns[6]);
												//console.log("arr[1]: " + arrayOfColumns[1]);
											}
										}
									}
								});
							} else {
								console.log("Error reading file: " + err);
							}
						});
					}
				});
	

}

exports.init = function(){
	//exports.parseUnicodeTextTableToNewProject("Helping Orphans Project 2", "Donations to the Poor", "Samer", "http://www.chinadaily.com.cn/china/images/attachement/jpg/site1/20111012/0013729e48090fff10e803.jpg");
}

exports.init();
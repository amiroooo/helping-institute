	$(function() {

		$("#add_project_form").validate({
			rules: {
				projectName: "required",
				ownerName: "required"
			},
			messages: {
				projectName: "You need to atleast fill the all obligatory fields.",
				ownerName: "You need to atleast fill the all obligatory fields."
			}
		});	
		
		$("#add_user_form").validate({
			rules: {
				userName: "required",
				userLocation: "required"
			},
			messages: {
				userName: "You need to atleast fill the all obligatory fields.",
				userLocation: "You need to atleast fill the all obligatory fields."
			}
		});	
		
		

		$("#add_transfer_form").validate({
			rules: {
				sum: {
				    required: true,
				    number: true
				 },
				description:{
					 required: true
				 }
			},
			messages: {
				sum: "You need to fill sum with sum amount (number).",
				description: "You need to give the transfer description."
			}
		});			
		
	});